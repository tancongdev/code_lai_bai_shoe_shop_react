import React from "react";
import Item from "./Item";
import { useState } from "react";

// const { data } = props;
const ListProduct = ({ data }) => {
  console.log("🚀 ~ file: ListProduct.jsx:6 ~ ListProduct ~ data:", data);
  const [productDetail, setproductDetail] = useState(data[1]);
  console.log(
    "🚀 ~ file: ListProduct.jsx:9 ~ ListProduct ~ productDetail:",
    productDetail
  );

  const handleprdDetail = (product) => {
    setproductDetail(product);
  };
  console.log(
    "🚀 ~ file: ListProduct.jsx:17 ~ handleprdDetail ~ handleprdDetail:",
    handleprdDetail
  );
  return (
    <div className="row">
      {data.map((item) => {
        return (
          <div key={item.id} className="col-4 pb-5">
            <div className="card">
              <img src={item.image} alt="..." />

              <div className="card-body font-weight-bold">
                <p>{item.name}</p>
                <p>{item.price}$</p>

                <button
                  type="button"
                  class=" bg-dark text-light"
                  data-toggle="modal"
                  data-target="#exampleModal"
                  onClick={() => handleprdDetail(item)}
                >
                  Chi tiết sản phẩm
                  {/* <i class="fa fa-cart-plus"></i> */}
                </button>

                <div
                  className="modal fade"
                  id="exampleModal"
                  tabIndex={-1}
                  aria-labelledby="exampleModalLabel"
                  aria-hidden="true"
                >
                  <div className="modal-dialog">
                    <div className="modal-content">
                      <div className="modal-header ">
                        <h5
                          className="modal-title  "
                          id="exampleModalLabel "
                        >
                          Chi tiết sản phẩm
                        </h5>
                        <button
                          type="button"
                          className="close"
                          data-dismiss="modal"
                          aria-label="Close"
                        >
                          <span aria-hidden="true">×</span>
                        </button>
                      </div>
                      <div className="modal-body">
                        <div className="row">
                          <div className="col-4 "></div>
                          <img
                            className="img-fluid"
                            src={productDetail.image}
                            alt="..."
                          />
                          <div className="col-8">
                            <p> Name: {productDetail.name}</p>
                            <p className="mt-3">
                              {" "}
                              Description:
                              <p> {productDetail.description}</p>
                            </p>
                            <p>Price: {productDetail.price}$</p>
                          </div>
                        </div>
                      </div>
                      <div className="modal-footer">
                        <button
                          type="button"
                          className="btn btn-secondary"
                          data-dismiss="modal"
                        >
                          Close
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      })}
      <Item />
    </div>
  );
};

export default ListProduct;
